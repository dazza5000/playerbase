package com.amicly.playerbase.base

class Constants {
    companion object {
        const val KEY_EVENT : String = "event"
        const val KEY_DATE : String = "date"
        const val KEY_NAME : String = "name"
    }
}