package com.amicly.playerbase.base.ui.picker

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.DatePicker
import com.amicly.playerbase.R
import java.util.*


class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var dateSelectedListener : DateSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(activity, R.style.AppTheme_DatePicker, this, year, month, day)
    }

    override fun onDateSet(datePicker: DatePicker, year: Int, month: Int, day: Int) {
        val year = datePicker.year
        val month = datePicker.month
        val day = datePicker.dayOfMonth
        val date = GregorianCalendar(year, month, day).time
        dateSelectedListener.onDateSelected(date)
    }

    fun setOnDateSelectedListener(dateSelectedListener: DateSelectedListener) {
        this.dateSelectedListener = dateSelectedListener
    }

    interface DateSelectedListener {
       fun onDateSelected(date : Date)
    }
}