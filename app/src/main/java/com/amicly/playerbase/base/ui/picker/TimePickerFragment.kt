package com.amicly.playerbase.base.ui.picker

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.TimePicker
import com.amicly.playerbase.R
import java.util.*


class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private lateinit var timeSelectedListener : TimeSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        return TimePickerDialog(activity, R.style.AppTheme_DatePicker, this, hour, minute, false)
    }

    override fun onTimeSet(datePicker: TimePicker?, hourOfDay: Int, minute: Int) {
        timeSelectedListener.onTimeSelected(hourOfDay, minute)
    }

    fun setOnTimeSelectedListener(timeSelectedListener: TimeSelectedListener) {
        this.timeSelectedListener = timeSelectedListener
    }

    interface TimeSelectedListener {
       fun onTimeSelected(hourOfDay: Int, minute: Int)
    }
}