package com.amicly.playerbase

import com.amicly.playerbase.di.component.DaggerApplicationComponent
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.fabric.sdk.android.Fabric
import timber.log.Timber
import com.google.firebase.firestore.FirebaseFirestoreSettings




/**
 * Created by darrankelinske on 1/24/18.
 */
class PlayerBaseApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        FirebaseAuth.getInstance().signInAnonymously()

        Fabric.with(this, Crashlytics())
        Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder().application(this).build()
    }
}