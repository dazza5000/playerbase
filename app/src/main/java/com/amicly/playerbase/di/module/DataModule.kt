package com.amicly.playerbase.di.module

import com.amicly.playerbase.di.scope.ActivityScoped
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by darrankelinske on 2/11/18.
 */

@ActivityScoped
@Module
class DataModule {

    @Provides
    @Singleton
    internal fun firestore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }
}