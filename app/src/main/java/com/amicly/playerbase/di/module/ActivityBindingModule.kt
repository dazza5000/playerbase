package com.amicly.playerbase.di.module

import com.amicly.playerbase.addevent.AddEditEventActivity
import com.amicly.playerbase.di.scope.ActivityScoped
import com.amicly.playerbase.events.EventsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector



/**
 * Created by darrankelinske on 2/11/18.
 */
@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [(EventsModule::class)])
    internal abstract fun eventsActivity(): EventsActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(EventsModule::class)])
    internal abstract fun addEditActivity(): AddEditEventActivity

}