package com.amicly.playerbase.di.module

import com.amicly.playerbase.addevent.AddEditEventActivity
import com.amicly.playerbase.addevent.AddEditEventContract
import com.amicly.playerbase.di.scope.ActivityScoped
import com.amicly.playerbase.events.EventsActivity
import com.amicly.playerbase.events.EventsContract
import dagger.Binds
import dagger.Module


/**
 * Created by darrankelinske on 2/11/18.
 */
@ActivityScoped
@Module
abstract class EventsModule {

    @Binds
    abstract fun eventsView(eventsActivity: EventsActivity): EventsContract.View

    @Binds
    abstract fun addEditEventView(addEditActivity : AddEditEventActivity): AddEditEventContract.View

}