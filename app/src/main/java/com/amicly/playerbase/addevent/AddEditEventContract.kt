package com.amicly.playerbase.addevent

import com.amicly.playerbase.base.mvp.BaseContract
import java.util.*

/**
 * Created by darrankelinske on 2/11/18.
 */
interface AddEditEventContract {
    interface Presenter {
        fun onEventTitleUpdated(eventTitle : String)
        fun onEventDescriptionChanged(eventDescription : String)
        fun selectDateClicked()
        fun onDateSelected(date: Date)
        fun selectTimeClicked()
        fun onTimeSelected(hourOfDay: Int, minute: Int)
        fun saveEvent()
    }
    interface View : BaseContract.View {
        fun showDatePicker()
        fun showTimePicker()
        fun finishAddEditEvent()
        fun showErrorMessage(errorMessage : String)
    }
}