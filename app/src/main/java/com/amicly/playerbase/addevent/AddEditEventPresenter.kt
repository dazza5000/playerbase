package com.amicly.playerbase.addevent

import android.text.TextUtils
import com.amicly.playerbase.base.Constants.Companion.KEY_EVENT
import com.amicly.playerbase.model.ModelClasses.Event
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber
import java.util.*
import javax.inject.Inject


/**
 * Created by darrankelinske on 2/16/18.
 */


class AddEditEventPresenter @Inject constructor() : AddEditEventContract.Presenter {

    @Inject lateinit var firestore : FirebaseFirestore
    @Inject lateinit var view : AddEditEventContract.View

    private val invalidFieldErrorMessage : String = "Not all fields are valid."

    private var eventTitle : String = ""
    private var eventDescription : String = ""
    private var date : Date = Date()
    private var hourOfDay: Int = 0
    private var minute: Int = 0

    override fun onEventTitleUpdated(eventTitle: String) {
        this.eventTitle = eventTitle
    }

    override fun onEventDescriptionChanged(eventDescription: String) {
        this.eventDescription = eventDescription
    }

    override fun selectDateClicked() {
        view.showDatePicker()
    }

    override fun onDateSelected(date: Date) {
        this.date = date
    }

    override fun selectTimeClicked() {
        view.showTimePicker()
    }

    override fun onTimeSelected(hourOfDay: Int, minute: Int) {
        this.hourOfDay = hourOfDay
        this.minute = minute
    }

    override fun saveEvent() {

        if (date.before(Date()) || hourOfDay == 0 || minute == 0 || TextUtils.isEmpty(eventTitle)
                || TextUtils.isEmpty(eventDescription)) {
            view.showErrorMessage(invalidFieldErrorMessage)
            Timber.e(invalidFieldErrorMessage)
            return
        }

        setHourAndMinuteOnDate(date, hourOfDay, minute)

        val firebaseFirestore = FirebaseFirestore.getInstance()

        val currentTime : String = System.currentTimeMillis().toString()

        val eventsDocumentRef =
                firebaseFirestore.collection(KEY_EVENT).document(currentTime)
        val testEvent = Event(eventTitle, eventDescription, date.time)

        eventsDocumentRef.set(testEvent).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null) {
                    Timber.e("DocumentSnapshot data: " + task.result)
                } else {
                    Timber.e("No such document")
                }
            } else {
                Timber.e("get failed with " + task.exception)
            }
            view.finishAddEditEvent()
        }
    }

    private fun setHourAndMinuteOnDate(date: Date, hourOfDay: Int, minute: Int) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)

        this.date = calendar.time
    }
}
