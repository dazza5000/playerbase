package com.amicly.playerbase.addevent

import android.app.ProgressDialog.show
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.amicly.playerbase.R
import com.amicly.playerbase.addevent.AddEditEventActivity.Companion.TAG_DATE_PICKER
import com.amicly.playerbase.addevent.AddEditEventActivity.Companion.TAG_TIME_PICKER
import com.amicly.playerbase.base.mvp.BaseActivity
import com.amicly.playerbase.base.ui.picker.DatePickerFragment
import com.amicly.playerbase.base.ui.picker.TimePickerFragment
import kotlinx.android.synthetic.main.activity_add_event.*
import java.util.*
import javax.inject.Inject


class AddEditEventActivity : BaseActivity(), AddEditEventContract.View {

    companion object {
        const val TAG_DATE_PICKER = "DATE_PICKER"
        const val TAG_TIME_PICKER = "TIME_PICKER"
    }

    @Inject lateinit var addEditEventPresenter : AddEditEventPresenter

    private lateinit var saveEventFAB : FloatingActionButton
    private lateinit var eventTitle : EditText
    private lateinit var eventDescription : EditText
    private lateinit var datePickerFragment: DatePickerFragment
    private lateinit var timePickerFragment: TimePickerFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_event)

        eventTitle = edit_text_event_title
        eventDescription = edit_text_event_description
        saveEventFAB = findViewById(R.id.fab_save_event)
        edit_text_event_date.setOnClickListener { addEditEventPresenter.selectDateClicked() }
        edit_text_event_time.setOnClickListener { addEditEventPresenter.selectTimeClicked() }


        eventTitle.addTextChangedListener(getEventTitleTextWatcher())
        eventDescription.addTextChangedListener(getEventDescriptionTextWatcher())

        fab_save_event.setOnClickListener { addEditEventPresenter
                .saveEvent() }
    }

    override fun finishAddEditEvent() {
        finish()
    }

    override fun showDatePicker() {
        datePickerFragment = DatePickerFragment()
        datePickerFragment.setOnDateSelectedListener(AddEditDateSelectedListener(addEditEventPresenter))
        datePickerFragment.show(supportFragmentManager, TAG_DATE_PICKER)
    }

    override fun showTimePicker() {
        timePickerFragment = TimePickerFragment()
        timePickerFragment.setOnTimeSelectedListener(AddEditTimeSelectedListener(addEditEventPresenter))
        timePickerFragment.show(supportFragmentManager, TAG_TIME_PICKER)

    }

    class AddEditDateSelectedListener(private val addEditEventPresenter: AddEditEventPresenter)
        : DatePickerFragment.DateSelectedListener {
        override fun onDateSelected(date: Date) {
            addEditEventPresenter.onDateSelected(date)
        }
    }

    class AddEditTimeSelectedListener(private val addEditEventPresenter: AddEditEventPresenter)
        : TimePickerFragment.TimeSelectedListener {
        override fun onTimeSelected(hourOfDay: Int, minute: Int) {
            addEditEventPresenter.onTimeSelected(hourOfDay, minute)
        }
    }

    private fun getEventTitleTextWatcher() : TextWatcher  {
        return object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                addEditEventPresenter.onEventTitleUpdated(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
    }

    private fun getEventDescriptionTextWatcher() : TextWatcher  {
        return object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                addEditEventPresenter.onEventDescriptionChanged(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
    }

    override fun showErrorMessage(errorMessage : String) {
        Snackbar.make(coordinator_layout_add_event, errorMessage, Snackbar.LENGTH_SHORT).show()
    }
}
