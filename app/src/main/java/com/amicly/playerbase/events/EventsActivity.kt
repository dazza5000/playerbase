package com.amicly.playerbase.events

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.amicly.playerbase.R
import com.amicly.playerbase.addevent.AddEditEventActivity
import com.amicly.playerbase.base.mvp.BaseActivity
import com.amicly.playerbase.model.ModelClasses
import kotlinx.android.synthetic.main.activity_add_event.*
import kotlinx.android.synthetic.main.activity_events.*
import javax.inject.Inject


class EventsActivity : BaseActivity(), EventsContract.View {

    @Inject lateinit var eventsPresenter: EventsPresenter

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var eventAdapter : EventsRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerview_events.layoutManager = linearLayoutManager
        eventAdapter = EventsRecyclerAdapter(ArrayList(), getEventClickListener())
        recyclerview_events.adapter = eventAdapter
        recyclerview_events.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        fab_add_event.setOnClickListener { onAddEventClick() }
    }

    private fun onAddEventClick() {
        startActivity(Intent(this, AddEditEventActivity::class.java))
    }

    override fun onResume() {
        super.onResume()
        eventsPresenter.loadEvents()
    }

    override fun showEvents(events: ArrayList<ModelClasses.Event>) {
        eventAdapter.setEvents(events)
    }

    override fun showErrorMessage(errorMessage : String) {
        Snackbar.make(coordinator_layout_event_activity, errorMessage, Snackbar.LENGTH_SHORT).show()
    }

    private fun getEventClickListener() : EventsRecyclerAdapter.EventClickListener {
        return object : EventsRecyclerAdapter.EventClickListener {
            override fun onEventClick(event: ModelClasses.Event) {
                showErrorMessage(event.name)
            }
        }
    }
}
