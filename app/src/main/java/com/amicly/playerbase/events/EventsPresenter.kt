package com.amicly.playerbase.events

import com.amicly.playerbase.base.Constants.Companion.KEY_DATE
import com.amicly.playerbase.base.Constants.Companion.KEY_EVENT
import com.amicly.playerbase.base.Constants.Companion.KEY_NAME
import com.amicly.playerbase.model.ModelClasses
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by darrankelinske on 2/11/18.
 */
class EventsPresenter @Inject constructor() : EventsContract.Presenter {

    @Inject lateinit var firestore : FirebaseFirestore
    @Inject lateinit var view : EventsContract.View
    private var events : ArrayList<ModelClasses.Event> = ArrayList()

    override fun loadEvents() {
        events.clear()
        val query = firestore
                .collection(KEY_EVENT)
                .whereGreaterThan(KEY_DATE, System.currentTimeMillis())
                .orderBy(KEY_DATE)
                .orderBy(KEY_NAME)

        query.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Timber.d("Task was successful")
                task.result.forEach{
                    events.add(it.toObject(ModelClasses.Event::class.java))
                }
                view.showEvents(events)
                Timber.i("The list size is: " +events.size);
            } else {
                Timber.d("Task was a failure: " + task.result)
            }
        }
    }
}