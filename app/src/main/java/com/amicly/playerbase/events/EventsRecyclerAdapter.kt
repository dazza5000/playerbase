package com.amicly.playerbase.events

import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import com.amicly.playerbase.R
import com.amicly.playerbase.events.EventsRecyclerAdapter.EventHolder
import com.amicly.playerbase.inflate
import com.amicly.playerbase.model.ModelClasses.Event
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import kotlinx.android.synthetic.main.item_event.view.*
import java.util.*


/**
 * Created by darrankelinske on 1/25/18.
 */
class EventsRecyclerAdapter(private var events: List<Event>,
                            private var eventClickListener : EventClickListener)
    : RecyclerView.Adapter<EventHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventHolder {
        val inflatedView = parent.inflate(R.layout.item_event, false)
        return EventHolder(inflatedView, eventClickListener)
    }

    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        val event = events[position]
        holder.bindEvent(event)
    }

    override fun getItemCount(): Int {
        return events.size
    }

    fun setEvents(events : List<Event>)  {
        this.events = events
        notifyDataSetChanged()
    }

    class EventHolder(private var view: View, private var eventClickListener: EventClickListener)
        : RecyclerView.ViewHolder(view) {

        private var event: Event? = null

        fun bindEvent(event: Event) {
            this.event = event
            view.eventName.text = event.name
            view.eventDescription.text = event.description
            view.eventDateTime.text = getDateText(event.date)
            view.eventDistance.text = "Earth"
            view.setOnClickListener({eventClickListener.onEventClick(event)})

            Glide.with(view)
                    .load("https://i.imgur.com/2VNGNyb.jpg")
                    .into(view.eventImage)
        }

        private fun getDateText(date: Long) : String {
            return DateFormat.format("MMMM dd yyyy, h:mm aa", date).toString()
        }
    }

    interface EventClickListener {
        fun onEventClick(event : Event)
    }
}