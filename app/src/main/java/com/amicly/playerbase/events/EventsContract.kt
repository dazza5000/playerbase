package com.amicly.playerbase.events

import com.amicly.playerbase.base.mvp.BaseContract
import com.amicly.playerbase.model.ModelClasses

/**
 * Created by darrankelinske on 2/11/18.
 */
interface EventsContract {
    interface Presenter {
        fun loadEvents()
    }
    interface View : BaseContract.View {
        fun showEvents(events: ArrayList<ModelClasses.Event>)
        fun showErrorMessage(errorMessage: String)
    }
}