package com.amicly.playerbase.model

/**
 * Created by darrankelinske on 1/24/18.
 */
class ModelClasses {
    data class Event(var name: String = "", var description: String = "", var date : Long = 777)
    data class Location(var latitude: Long, var longitude : Long, var name : String)
}